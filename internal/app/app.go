package app

import (
	"net"
	"time"

	"gitlab.com/nharward/dyndns-gclouddns/internal/gdns"
)

type ExternalIP interface {
	GetExternalIPv4() (net.IP, error)
	GetExternalIPv6() (net.IP, error)
}

// Events are fired asynchronously in individual go-routines
type EventListener interface {
	IPv4(previous, updated net.IP, err error)
	IPv6(previous, updated net.IP, err error)
}

type App struct {
	externalIP ExternalIP
	host       gdns.GoogleDNSHost
	poll       time.Duration
	event      *EventListener
}

// Run will run the main program until an unrecoverable error is encounted (if any)
func (app App) Run() error {
	for {
		registeredIPv4s, registeredIPv6s, err := app.host.GetIPAddresses()
		if err != nil {
			if app.event != nil {
				(*app.event).IPv4(nil, nil, err)
				(*app.event).IPv6(nil, nil, err)
			}
		} else {
			var registeredIPv4, registeredIPv6 net.IP
			if len(registeredIPv4s) > 0 {
				registeredIPv4 = registeredIPv4s[0]
			}
			if len(registeredIPv6s) > 0 {
				registeredIPv6 = registeredIPv6s[0]
			}

			// Get current IPv4 addresses
			actualIPv4, ipv4Err := app.externalIP.GetExternalIPv4()
			if ipv4Err != nil {
				actualIPv4 = registeredIPv4
			}

			// Get current IPv6 addresses
			actualIPv6, ipv6Err := app.externalIP.GetExternalIPv6()
			if ipv6Err != nil {
				actualIPv6 = registeredIPv6
			}

			// Update DNS
			err = app.host.SetIPAddresses(actualIPv4, actualIPv6)

			// Fire callbacks
			if app.event != nil {
				if ipv4Err == nil && err != nil {
					ipv4Err = err
				}
				if ipv6Err == nil && err != nil {
					ipv6Err = err
				}
				go func() { (*app.event).IPv4(registeredIPv4, actualIPv4, ipv4Err) }()
				go func() { (*app.event).IPv6(registeredIPv6, actualIPv6, ipv6Err) }()
			}
		}

		time.Sleep(app.poll)
	}
}

func New(host gdns.GoogleDNSHost, externalIP ExternalIP, pollDuration time.Duration, listener *EventListener) App {
	return App{
		externalIP: externalIP,
		host:       host,
		poll:       pollDuration,
		event:      listener,
	}
}
