package test

import (
	"fmt"
	"testing"
)

// Suite is a collection of test cases to be run as a group.
type Suite struct {
	t *testing.T
}

// TestCase evaluates the test case, if failure then it's logged with formatString and args.
func (suite *Suite) TestCase(passed bool, format string, args ...interface{}) {
	if !passed {
		suite.t.Fatal(fmt.Sprintf(format, args...))
	}
}

// NewSuite returns an empty test suite.
func NewSuite(t *testing.T) *Suite {
	return &Suite{t: t} // cases: []Case{}}
}
