package gdns

import (
	"context"
	"fmt"
	"net"

	"google.golang.org/api/dns/v1"
)

type GoogleDNSHost interface {
	// GetIPAddresses returns IPv4 and IPv4 addresses for the host, or an error.
	GetIPAddresses() (ipv4IPs, ipv6IPs []net.IP, err error)

	// SetIPAddresses sets the IPv4 and IPv4 addresses for the host if they differ from what is currently in Google Cloud DNS, including removing them if they are nil.
	SetIPAddresses(ipv4, ipv6 net.IP) error
}

// Our implementation of GoogleDNSHost
type googleDNSHost struct {
	dnsService              *dns.Service
	hostname, project, zone string
	ttl                     int64
}

// resourceRecordSets returns the A (IPv4) and AAAA (IPv6) record sets for the host
func (host *googleDNSHost) resourceRecordSets() (a, aaaa *dns.ResourceRecordSet, err error) {
	rrsService := dns.NewResourceRecordSetsService(host.dnsService)
	var nextPageToken *string
	for a == nil || aaaa == nil {
		listCall := rrsService.List(host.project, host.zone)
		if nextPageToken != nil {
			listCall = listCall.PageToken(*nextPageToken)
		}
		response, err := listCall.Do()
		if err != nil {
			return nil, nil, err
		}
		nextPageToken = nil
		if len(response.NextPageToken) > 0 {
			nextPageToken = &response.NextPageToken
		}
		for _, rrs := range response.Rrsets {
			if rrs.Name == host.hostname {
				if rrs.Type == `A` {
					a = rrs
				} else if rrs.Type == `AAAA` {
					aaaa = rrs
				}
			}
		}
		if nextPageToken == nil {
			// No more results to go through
			break
		}
	}
	return a, aaaa, err
}

// GetIPAddresses returns IPv4 and IPv4 addresses for the host, or an error
func (host *googleDNSHost) GetIPAddresses() (ipv4s, ipv6s []net.IP, err error) {
	ipv4RRS, ipv6RRS, err := host.resourceRecordSets()
	ipv4s, ipv6s = []net.IP{}, []net.IP{}
	if err != nil {
		return nil, nil, err
	}
	if ipv4RRS != nil && ipv4RRS.Rrdatas != nil {
		for _, ipv4String := range ipv4RRS.Rrdatas {
			if ipv4 := net.ParseIP(ipv4String); ipv4 != nil {
				ipv4s = append(ipv4s, ipv4)
			}
		}
	}
	if ipv6RRS != nil && ipv6RRS.Rrdatas != nil {
		for _, ipv6String := range ipv6RRS.Rrdatas {
			if ipv6 := net.ParseIP(ipv6String); ipv6 != nil {
				ipv6s = append(ipv6s, ipv6)
			}
		}
	}
	return ipv4s, ipv6s, nil
}

func (host *googleDNSHost) getChanges(ip net.IP, rrs *dns.ResourceRecordSet) (addition, deletion *dns.ResourceRecordSet) {
	if ip == nil && rrs == nil {
		// Nothing to do
		addition = nil
		deletion = nil
	} else if ip == nil && rrs != nil {
		// Remove existing record
		addition = nil
		deletion = rrs
	} else if ip != nil && rrs == nil {
		// Add a new DNS record
		ipType := `A`
		if ip != nil && ip.To4() == nil {
			ipType = `AAAA`
		}
		addition = &dns.ResourceRecordSet{
			Kind:    `dns#resourceRecordSet`,
			Name:    host.hostname,
			Rrdatas: []string{ip.String()},
			Ttl:     host.ttl,
			Type:    ipType,
		}
		deletion = nil
	} else {
		if len(rrs.Rrdatas) == 1 && rrs.Rrdatas[0] == ip.String() {
			// IP is the same, no update
			addition = nil
			deletion = nil
		} else {
			// Change existing record
			addition = &dns.ResourceRecordSet{
				Kind:    rrs.Kind,
				Name:    rrs.Name,
				Rrdatas: []string{ip.String()},
				Ttl:     rrs.Ttl,
				Type:    rrs.Type,
			}
			deletion = rrs
		}
	}
	return
}

// Host returns a new GoogleDNSHost describing the given hostname which may or may not have IPv4/IPv6 addresses already. Exactly one of *GoogleDNSHost or error will be non-nil.
func Host(ctx context.Context, google_project, managed_zone, hostname string, ttl int64) (*GoogleDNSHost, error) {
	host := &googleDNSHost{
		hostname: fmt.Sprintf(`%s.`, hostname),
		project:  google_project,
		ttl:      ttl,
		zone:     managed_zone,
	}
	var err error
	if host.dnsService, err = dns.NewService(ctx); err != nil {
		return nil, err
	}
	iface := GoogleDNSHost(host)
	return &iface, nil
}

func (host *googleDNSHost) SetIPAddresses(ipv4, ipv6 net.IP) error {
	// Ensure our IPv4 and IPv6 addresses are valid (or nil)
	if err := validIPs(ipv4, ipv6); err != nil {
		return err
	}

	ipv4RRS, ipv6RRS, err := host.resourceRecordSets()
	if err != nil {
		return err
	}

	change := dns.Change{
		Additions: []*dns.ResourceRecordSet{},
		Deletions: []*dns.ResourceRecordSet{},
		IsServing: true,
		Kind:      `dns#change`,
	}

	ipv4Add, ipv4Delete := host.getChanges(ipv4, ipv4RRS)
	ipv6Add, ipv6Delete := host.getChanges(ipv6, ipv6RRS)

	for _, added := range []*dns.ResourceRecordSet{ipv4Add, ipv6Add} {
		if added != nil {
			change.Additions = append(change.Additions, added)
		}
	}
	for _, deleted := range []*dns.ResourceRecordSet{ipv4Delete, ipv6Delete} {
		if deleted != nil {
			change.Deletions = append(change.Deletions, deleted)
		}
	}

	if _, err := dns.NewChangesService(host.dnsService).Create(host.project, host.zone, &change).Do(); err != nil {
		return err
	}
	return nil
}

func validIPs(ipv4, ipv6 net.IP) error {
	if ipv4 != nil && ipv4.To4() == nil {
		return fmt.Errorf(`%s is not a valid IPv4 address`, ipv4.String())
	}
	if ipv6 != nil && ipv6.To4() != nil {
		return fmt.Errorf(`%s is not a valid IPv6 address`, ipv6.String())
	}
	return nil
}
