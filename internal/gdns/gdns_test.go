package gdns

import (
	"fmt"
	"net"
	"testing"

	"gitlab.com/nharward/dyndns-gclouddns/internal/test"
	"google.golang.org/api/dns/v1"
)

var (
	good4, good6 = net.ParseIP(`127.0.0.1`), net.ParseIP(`::1`)
	goodIPs      = []net.IP{good4, good6}
)

func newHost() *googleDNSHost {
	return &googleDNSHost{}
}

func TestPrerequisites(t *testing.T) {
	suite := test.NewSuite(t)
	suite.TestCase(good4 != nil, `Valid IPv4 address should be non-nil`)
	suite.TestCase(good6 != nil, `Valid IPv6 address should be non-nil`)
}

func TestValid(t *testing.T) {
	suite := test.NewSuite(t)

	// Positive test cases
	suite.TestCase(validIPs(nil, nil) == nil, `both nil`)
	suite.TestCase(validIPs(nil, good6) == nil, `ipv4 nil`)
	suite.TestCase(validIPs(good4, nil) == nil, `ipv6 nil`)
	suite.TestCase(validIPs(good4, good6) == nil, `both valid`)

	// Negative cases
	suite.TestCase(validIPs(good6, nil) != nil, `bad ipv4`)
	suite.TestCase(validIPs(nil, good4) != nil, `bad ipv6`)
	suite.TestCase(validIPs(good6, good4) != nil, `both bad`)

}

func TestGetChanges_NoChangeBothNil(t *testing.T) {
	suite := test.NewSuite(t)
	addition, deletion := newHost().getChanges(nil, nil)
	suite.TestCase(addition == nil, `Addition should be nil`)
	suite.TestCase(deletion == nil, `Deletion should be nil`)
}

func TestGetChanges_NoChangeBothNonNil(t *testing.T) {
	suite := test.NewSuite(t)
	for _, ip := range goodIPs {
		addition, deletion := newHost().getChanges(ip, &dns.ResourceRecordSet{Rrdatas: []string{ip.String()}})
		suite.TestCase(addition == nil, `[%s] There should be no additions`, ip)
		suite.TestCase(deletion == nil, `[%s] There should be no deletions`, ip)
	}
}

func TestGetChanges_AddressAdded(t *testing.T) {
	suite := test.NewSuite(t)
	for _, ip := range goodIPs {
		addition, deletion := newHost().getChanges(ip, nil)
		additionHasSingleIP := addition != nil && addition.Rrdatas != nil && len(addition.Rrdatas) == 1
		suite.TestCase(additionHasSingleIP, `[%s] Should be exactly 1 addition with a single IP`, ip)
		if additionHasSingleIP {
			suite.TestCase(addition.Rrdatas[0] == ip.String(), `[%s] IP addresses should match: expected[%s], actual[%s]`, ip, ip, addition.Rrdatas[0])
			if ip.To4() != nil {
				suite.TestCase(addition.Type == `A`, `[%s] IPv4 address should have type 'A', actual[%s]`, ip, addition.Type)
			} else {
				suite.TestCase(addition.Type == `AAAA`, `[%s] IPv6 address should have type 'AAAA', actual[%s]`, ip, addition.Type)
			}
		}
		suite.TestCase(deletion == nil, `[%s] Deletion should be nil`, ip)
	}
}

func TestGetChanges_AddressDeleted(t *testing.T) {
	suite := test.NewSuite(t)
	for _, ip := range goodIPs {
		rrsType := `A`
		if ip.To4() == nil {
			rrsType = `AAAA`
		}
		rrs := &dns.ResourceRecordSet{
			Rrdatas: []string{ip.String()},
			Kind:    `somekind`,
			Name:    `somename`,
			Ttl:     int64(12345),
			Type:    rrsType,
		}
		rrsCopy := &dns.ResourceRecordSet{
			Rrdatas: []string{ip.String()},
			Kind:    `somekind`,
			Name:    `somename`,
			Ttl:     int64(12345),
			Type:    rrsType,
		}
		addition, deletion := newHost().getChanges(nil, rrs)
		deletionHasSingleIP := deletion != nil && deletion.Rrdatas != nil && len(deletion.Rrdatas) == 1

		suite.TestCase(addition == nil, `[%s] There should be no addition`, ip)
		suite.TestCase(deletionHasSingleIP, `[%s] There should be exactly one deletion with a single IP`, ip)

		// Make sure the deleted RRS matches *exactly* what was passed in, per the Google SDK
		suite.TestCase(deletion.Rrdatas[0] == rrsCopy.Rrdatas[0], `[%s] IP addresses should match: expected[%s], actual[%s]`, ip, rrsCopy.Rrdatas[0], deletion.Rrdatas[0])
		suite.TestCase(deletion.Kind == rrsCopy.Kind, `[%s] Kind should match: expected[%s], actual[%s]`, ip, rrsCopy.Kind, deletion.Kind)
		suite.TestCase(deletion.Name == rrsCopy.Name, `[%s] Name should match: expected[%s], actual[%s]`, ip, rrsCopy.Name, deletion.Name)
		suite.TestCase(deletion.Ttl == rrsCopy.Ttl, `[%s] Ttl should match: expected[%d], actual[%d]`, ip, rrsCopy.Ttl, deletion.Ttl)
		suite.TestCase(deletion.Type == rrsCopy.Type, `[%s] Type should match: expected[%s], actual[%s]`, ip, rrsCopy.Type, deletion.Type)
	}
}

type change struct {
	oldIP net.IP
	newIP net.IP
}

func (c change) String() string {
	return fmt.Sprintf(`%s->%s`, c.oldIP, c.newIP)
}

func TestGetChanges_AddressChanged(t *testing.T) {
	suite := test.NewSuite(t)

	for _, change := range []change{
		{oldIP: net.ParseIP(`192.168.1.1`), newIP: net.ParseIP(`10.0.0.1`)}, // IPv4
		{oldIP: net.ParseIP(`::1`), newIP: net.ParseIP(`::2`)},              // IPv6
	} {
		rrsType := `A`
		if change.oldIP.To4() == nil {
			rrsType = `AAAA`
		}
		rrs := &dns.ResourceRecordSet{
			Rrdatas: []string{change.oldIP.String()},
			Kind:    `somekind`,
			Name:    `somename`,
			Ttl:     int64(12345),
			Type:    rrsType,
		}
		rrsCopy := &dns.ResourceRecordSet{
			Rrdatas: []string{change.oldIP.String()},
			Kind:    `somekind`,
			Name:    `somename`,
			Ttl:     int64(12345),
			Type:    rrsType,
		}
		addition, deletion := newHost().getChanges(change.newIP, rrs)

		suite.TestCase(addition != nil, `[%s] Addition cannot be nil`, change)
		suite.TestCase(addition.Rrdatas != nil && len(addition.Rrdatas) == 1, `[%s] Addition must have exactly 1 record`, change)
		suite.TestCase(addition.Rrdatas[0] == change.newIP.String(), `[%s] New IP address: expected[%s], actual[%s]`, change, change.newIP, addition.Rrdatas[0])
		suite.TestCase(addition.Kind == rrsCopy.Kind, `[%s] Addition Kind: expected[%s], actual[%s]`, change, rrsCopy.Kind, addition.Kind)
		suite.TestCase(addition.Name == rrsCopy.Name, `[%s] Addition Name: expected[%s], actual[%s]`, change, rrsCopy.Name, addition.Name)
		suite.TestCase(addition.Ttl == rrsCopy.Ttl, `[%s] Addition Ttl: expected[%d], actual[%d]`, change, rrsCopy.Ttl, addition.Ttl)
		suite.TestCase(addition.Type == rrsCopy.Type, `[%s] Addition Type: expected[%s], actual[%s]`, change, rrsCopy.Type, addition.Type)

		suite.TestCase(deletion != nil, `[%s] Deletion cannot be nil`, change)
		suite.TestCase(deletion.Rrdatas != nil && len(deletion.Rrdatas) == 1, `[%s] Deletion must have exactly 1 record`, change)
		suite.TestCase(deletion.Rrdatas[0] == change.oldIP.String(), `[%s] Old IP address: expected[%s], actual[%s]`, change, change.oldIP, deletion.Rrdatas[0])
		suite.TestCase(deletion.Kind == rrsCopy.Kind, `[%s] Deletion Kind: expected[%s], actual[%s]`, change, rrsCopy.Kind, deletion.Kind)
		suite.TestCase(deletion.Name == rrsCopy.Name, `[%s] Deletion Name: expected[%s], actual[%s]`, change, rrsCopy.Name, deletion.Name)
		suite.TestCase(deletion.Ttl == rrsCopy.Ttl, `[%s] Deletion Ttl: expected[%d], actual[%d]`, change, rrsCopy.Ttl, deletion.Ttl)
		suite.TestCase(deletion.Type == rrsCopy.Type, `[%s] Deletion Type: expected[%s], actual[%s]`, change, rrsCopy.Type, deletion.Type)
	}
}
