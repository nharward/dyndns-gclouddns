package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"time"

	"gitlab.com/nharward/dyndns-gclouddns/internal/app"
	"gitlab.com/nharward/dyndns-gclouddns/internal/gdns"
)

const (
	programError = iota + 1
	userError
)

var (
	programName string = "unknown program name, check build linker args" // Set through linker option -X main.programName=...
	branch      string = "unknown branch, check build linker args"       // Set through linker option -X main.branch=...
	commit      string = "unknown commit, check build linker args"       // Set through linker option -X main.commit=...
)

type ipLookup struct {
	ipv4URL, ipv6URL string
}

func (ipl ipLookup) GetExternalIPv4() (net.IP, error) {
	return ipl.getExternalIP(ipl.ipv4URL)
}

func (ipl ipLookup) GetExternalIPv6() (net.IP, error) {
	return ipl.getExternalIP(ipl.ipv6URL)
}

func (ipl ipLookup) getExternalIP(url string) (net.IP, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	content, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	ip := net.ParseIP(string(content))
	if ip == nil {
		return nil, fmt.Errorf(`Unable to parse IP address from string '%s'`, string(content))
	}
	return ip, nil
}

type event struct {
	Error      string    `json:"error,omitempty"`
	IPVersion  int       `json:"ip-type"`
	PreviousIP string    `json:"previous-ip,omitempty"`
	Timestamp  time.Time `json:"time"`
	UpdatedIP  string    `json:"updated-ip,omitempty"`
}

func newEvent(ipVersion int, previous, updated net.IP, err error) *event {
	evt := event{
		IPVersion: ipVersion,
		Timestamp: time.Now().Local(),
	}
	if err != nil {
		evt.Error = err.Error()
	}
	if previous != nil {
		evt.PreviousIP = previous.String()
	}
	if updated != nil {
		evt.UpdatedIP = updated.String()
	}
	return &evt
}

type eventListener struct {
	jsonOutput bool
}

func (el eventListener) IPv4(previous, updated net.IP, err error) {
	el.onEvent(newEvent(4, previous, updated, err))
}

func (el eventListener) IPv6(previous, updated net.IP, err error) {
	el.onEvent(newEvent(6, previous, updated, err))
}

func (el eventListener) onEvent(evt *event) {
	if el.jsonOutput {
		if err := json.NewEncoder(os.Stdout).Encode(evt); err != nil {
			fmt.Printf(`{"error":"Unable to format event in JSON: %s"}%s`, err.Error(), "\n")
		}
	} else {
		if len(evt.Error) > 0 {
			fmt.Printf("%s IPv%d error: %s\n", evt.Timestamp, evt.IPVersion, evt.Error)
		} else {
			if evt.PreviousIP != evt.UpdatedIP {
				fmt.Printf("%s IPv%d updated from %s to %s\n", evt.Timestamp, evt.IPVersion, evt.PreviousIP, evt.UpdatedIP)
			} else {
				fmt.Printf("%s IPv%d %s not updated\n", evt.Timestamp, evt.IPVersion, evt.PreviousIP)
			}
		}
	}
}

func main() {
	hostname := flag.String(`host`, ``, `The hostname to update with current IP information`)
	ip4URL := flag.String(`4`, `http://ipv4.whatismyip.akamai.com/`, `URL that returns the IPv4 address of the caller`)
	ip6URL := flag.String(`6`, `http://ipv6.whatismyip.akamai.com/`, `URL that returns the IPv6 address of the caller`)
	jsonOutput := flag.Bool(`json`, false, `Print output as JSON lines`)
	pollDuration := flag.Duration(`poll`, time.Hour, `How often to check for changes in external IP address`)
	project := flag.String(`project`, ``, `The Google project to use`)
	showVersion := flag.Bool(`version`, false, `Print version information and exit`)
	ttl := flag.Duration(`ttl`, time.Hour, `TTL for new A/AAAA records`)
	zone := flag.String(`zone`, ``, `The managed DNS zone name to use`)
	flag.Parse()

	if flag.NArg() != 0 {
		fmt.Fprintf(os.Stderr, "unknown arguments %v, please run again with -help\n", os.Args[1:])
		os.Exit(userError)
	}
	if *showVersion {
		fmt.Printf("%s version %s (commit %s)\n", programName, branch, commit)
		os.Exit(userError)
	}

	host, err := gdns.Host(context.Background(), *project, *zone, *hostname, int64(ttl.Seconds()))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to access host information from Google Cloud DNS: %v\n", err)
		os.Exit(programError)
	}

	ipLookup := ipLookup{
		ipv4URL: *ip4URL,
		ipv6URL: *ip6URL,
	}

	listener := app.EventListener(eventListener{jsonOutput: *jsonOutput})

	app := app.New(*host, ipLookup, *pollDuration, &listener)
	if err := app.Run(); err != nil {
		fmt.Fprintf(os.Stderr, "Unrecoverable error: %v\n", err)
		os.Exit(programError)
	}
}
