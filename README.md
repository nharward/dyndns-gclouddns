# dyndns-gclouddns

Dynamic DNS client backed by Google Cloud DNS. Why? I needed a dynamic DNS client that would run on a ~~[Netgear R6220](https://www.netgear.com/home/products/networking/wifi-routers/R6220.aspx)~~ Netgear R7000 running [OpenWRT](https://openwrt.org/) and couldn't find what I needed, so I wrote this.

# Installation

#### Prerequisites

- [Go](https://golang.org/) version 1.14+

#### Instructions

From the command line, run `go get gitlab.com/nharward/dyndns-gclouddns/cmd/ddns-gc`. The `ddns-gc` binary should now be located at `${GOPATH:-${HOME}/go}/bin` - add this to your `PATH` or place the binary in your `PATH` to run.

# Usage

#### Prerequisites

- A Google Cloud project with a Cloud DNS managed zone (you need the project ID and name of the zone)
- A JSON credentials file with at least - but preferably only - the _DNS Administrator_ role; see [Google's docs](https://cloud.google.com/docs/authentication/getting-started) for more information

#### Running

1. Export the `GOOGLE_APPLICATION_CREDENTIALS` environment variable to point to your JSON credentials file
2. Run `ddns-gc -host myhost.example.com -project <your-google-project-name> -zone <your-managed-zone-name>`, replacing `myhost.example.com` with the name you'd like your host to have in DNS

For a full list of command line options and default values, run with the `-help` option.

# Developing

#### Prerequisites

- [Go](https://golang.org/) version 1.14+
- [tup](http://gittup.org/tup/) - build system
- [Bash](https://www.gnu.org/software/bash/) - for use with `scripts/`

#### Initial setup

Run `tup init` at the top level of the project. This only needs to be done once, will create a `.tup` directory, and will allow `tup` to run from anywhere inside the repository tree.

#### Building

After making any changes simply run `tup` with no arguments. This will runs tests, `go vet`, and build the resulting binaries under `cmd/`.

If changing any of the `tup` build files you should run the following once you are finished:

1. `./clean.sh`
2. `tup scan`
3. `tup generate build.sh`

The last step will re-generate `build.sh` which is used in the CI pipeline to run tests, `go vet`, build, etc.

#### Visualizing the build

If you have [Graphviz/dot](https://graphviz.org/) installed, `tup` will give you a nice visualization of the entire build process and all dependencies. By default it only shows what needs to be done according to its internal database. To see the whole build you need to get the tree to a clean state:
1. Run `./clean.sh` - remove all build artifacts
2. Run `tup scan` - this will cause `tup` to notice build artifacts are now gone and need to be rebuilt, and propagate that information to its internal database
3. Finally, visualize the build graph (I use [imv](https://github.com/eXeC64/imv)): `tup graph --stickies | dot -o/dev/stdout -Tpng /dev/stdin | imv -`
