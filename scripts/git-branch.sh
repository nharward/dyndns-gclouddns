#! /usr/bin/env bash

branch="$(git rev-parse --abbrev-ref HEAD)"

if [[ "${branch}" = "master" ]]; then
    # If on master, use a tag if one exists
    if git describe --exact-match HEAD >/dev/null 2>/dev/null; then
        branch="$(git describe --exact-match HEAD)"
    fi
else
    # Replace '/' with '-' so the branch name can be used with file systems
    branch="${branch/\//-}"
fi

echo "${branch}"
