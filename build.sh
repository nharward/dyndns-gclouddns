#! /bin/sh -e
cd "cmd/ddns-gc"
(env GOPRIVATE=gitlab.com/nharward/dyndns-gclouddns go vet main.go > .vet_results)
cd "../.."
(env GOPRIVATE=gitlab.com/nharward/dyndns-gclouddns go test -cover gitlab.com/nharward/dyndns-gclouddns/internal/app       gitlab.com/nharward/dyndns-gclouddns/internal/gdns       gitlab.com/nharward/dyndns-gclouddns/internal/test > .test_results)
(env GOPRIVATE=gitlab.com/nharward/dyndns-gclouddns go vet gitlab.com/nharward/dyndns-gclouddns/internal/app       gitlab.com/nharward/dyndns-gclouddns/internal/gdns       gitlab.com/nharward/dyndns-gclouddns/internal/test > .vet_results)
cd "cmd/ddns-gc"
(env GOPRIVATE=gitlab.com/nharward/dyndns-gclouddns GOOS=linux GOARCH=arm GOARM=5 go build -v -ldflags="-w -s -X main.programName=ddns-gc -X main.branch=`../../scripts/git-branch.sh` -X main.commit=`../../scripts/git-commit.sh`" -o ddns-gc.arm main.go)
(env GOPRIVATE=gitlab.com/nharward/dyndns-gclouddns go build -v -ldflags="-w -s -X main.programName=ddns-gc -X main.branch=`../../scripts/git-branch.sh` -X main.commit=`../../scripts/git-commit.sh`" -o ddns-gc main.go)
